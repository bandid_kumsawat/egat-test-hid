import React from 'react';
import './App.css';

function App() {
  console.log( process.env )
  return (
    <div className="App">
      Example { `${process.env.REACT_APP_USERNAME} + ${process.env.REACT_APP_PASSWORD}` }
    </div>
  );
}

export default App;
